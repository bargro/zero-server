package pl.bar.gron.zero;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import pl.bar.gron.zero.players.entity.PlayerNotFoundException;

@Provider
public class PlayerNotFoundMapper implements ExceptionMapper<PlayerNotFoundException> {

    @Override
    public Response toResponse(PlayerNotFoundException e) {
        return Response.status(Response.Status.BAD_REQUEST)
                       .entity(e.getMessage())
                       .build();
    }
}