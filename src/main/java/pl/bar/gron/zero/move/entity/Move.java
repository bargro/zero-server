package pl.bar.gron.zero.move.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Move {
    private Integer chosenCardKey;
    private Integer rejectedCardKey;

}
