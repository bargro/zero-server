package pl.bar.gron.zero.move.boundary;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static pl.bar.gron.zero.game.entity.GameStage.END;
import static pl.bar.gron.zero.game.entity.GameStage.WAITING_FOR_PLAYERS;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import pl.bar.gron.zero.game.control.GameStateService;
import pl.bar.gron.zero.move.control.MoveService;
import pl.bar.gron.zero.move.entity.Move;
import pl.bar.gron.zero.players.control.PlayersService;
import pl.bar.gron.zero.players.entity.Player;;

@Path("move")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class MoveResource {

    @Inject
    private PlayersService playersService;

    @Inject
    private GameStateService stateService;

    @Inject
    private MoveService moveService;

    @POST
    @Path("{playerUuid}")
    public Response move(@PathParam("playerUuid") String playerUuid, Move move) {
        validate(playerUuid);

        moveService.move(move);

        return Response.ok()
                       .build();
    }

    @POST
    @Path("{playerUuid}/pas")
    public Response pas(@PathParam("playerUuid") String playerUuid) {
        validate(playerUuid);

        moveService.pas();

        return Response.ok()
                       .build();
    }

    private void validate(String playerUuid) {
        Player player = playersService.getPlayer(playerUuid)
                                      .orElseThrow(() -> new BadRequestException(
                                              "Nie ma takiego gracza!!!"));

        if (player.getUuid() != playersService.getCurrentPlayer()) {
            throw new BadRequestException("Nie wykonujesz teraz ruchu");
        }

        if (WAITING_FOR_PLAYERS == stateService.getGameStage()) {
            throw new BadRequestException("Gra się jeszcze nie zaczęła");
        }
        if (END == stateService.getGameStage()) {
            throw new BadRequestException("Gra się zakończyła");
        }
    }

}
