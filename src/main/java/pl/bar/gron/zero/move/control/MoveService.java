package pl.bar.gron.zero.move.control;

import javax.inject.Inject;

import lombok.extern.slf4j.Slf4j;
import pl.bar.gron.zero.cards.entity.Card;
import pl.bar.gron.zero.game.control.GameStateService;
import pl.bar.gron.zero.move.entity.Move;
import pl.bar.gron.zero.players.control.PlayersService;

@Slf4j
public class MoveService {
    @Inject
    private PlayersService playersService;
    @Inject
    private GameStateService gameStateService;

    public void move(Move move) {
        log.info("MOVE: {}", move);
        Card rejectedCard = playersService.getCurrentPlayerCard(move.getRejectedCardKey());
        Card chosenCard = gameStateService.getCardFromTable(move.getChosenCardKey());

        playersService.onMove(move.getRejectedCardKey(), chosenCard);
        gameStateService.onMove(move.getChosenCardKey(), rejectedCard);

        playersService.nextPlayer();
    }

    public void pas() {
        log.info("PAS");
        playersService.pas();
        if (playersService.isEnoughPasToEnd()) {
            gameStateService.endGame();
        }
        playersService.nextPlayer();
    }

}