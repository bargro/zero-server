package pl.bar.gron.zero.game.boundary;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import pl.bar.gron.zero.game.control.GameStateService;
import pl.bar.gron.zero.game.entity.GameState;
import pl.bar.gron.zero.players.control.PlayersService;
import pl.bar.gron.zero.players.entity.Player;
import pl.bar.gron.zero.players.entity.PlayerNotFoundException;

@Path("game")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class GameStateResource {

    @Inject
    private PlayersService playersService;

    @Inject
    private GameStateService gameStateService;

    @GET
    @Path("{playerUuid}")
    public Response getGameState(@PathParam("playerUuid") String playerUuid) {
        Player player = playersService.getPlayer(playerUuid)
                                      .orElseThrow(() -> new PlayerNotFoundException(
                                              "Nie ma takiego gracza!!!"));
        GameState state = gameStateService.getStateByPlayer(player);
        return Response.ok()
                       .entity(state)
                       .build();
    }
}
