package pl.bar.gron.zero.game.control;

import static pl.bar.gron.zero.game.entity.GameStage.END;
import static pl.bar.gron.zero.game.entity.GameStage.GAME;
import static pl.bar.gron.zero.game.entity.GameStage.WAITING_FOR_PLAYERS;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import pl.bar.gron.zero.cards.control.CardDrawer;
import pl.bar.gron.zero.cards.entity.Card;
import pl.bar.gron.zero.game.entity.GameStage;
import pl.bar.gron.zero.game.entity.GameState;
import pl.bar.gron.zero.players.control.PlayersService;
import pl.bar.gron.zero.players.entity.OtherPlayer;
import pl.bar.gron.zero.players.entity.Player;

@ApplicationScoped
public class GameStateService {
    @Inject
    private PlayersService playersService;

    @Inject
    private CardDrawer cardDrawer;

    private GameState currentState;

    @PostConstruct
    public void init() {
        prepareBeginState();
    }

    public GameStage getGameStage() {
        return currentState.getStage();
    }

    private void prepareBeginState() {
        currentState = GameState.builder()
                                .stage(WAITING_FOR_PLAYERS)
                                .cardsOnTable(new HashMap<>())
                                .build();
    }

    public GameState getStateByPlayer(Player player) {
        GameState gameState = new GameState(currentState);

        List<OtherPlayer> otherPlayers = playersService.getOtherPlayers(player);
        gameState.setPlayer(player);
        gameState.setOtherPlayers(otherPlayers);
        gameState.setCurrentPlayerId(playersService.getCurrentPlayer());

        return gameState;
    }

    public void beginGame() {
        playersService.onBeginGame();
        currentState.setStage(GAME);
        currentState.setCardsOnTable(cardDrawer.drawCardsForTable());
        currentState.setCurrentPlayerId(playersService.getCurrentPlayer());
    }

    public Card getCardFromTable(Integer cardKey) {
        return currentState.getCardsOnTable()
                           .get(cardKey);
    }

    public void onMove(Integer chosenCardKey, Card rejectedCard) {
        Map<Integer, Card> cardsOnTable = currentState.getCardsOnTable();
        cardsOnTable.put(chosenCardKey, rejectedCard);
    }

    public void endGame() {
        currentState.setStage(END);
        currentState.setCurrentPlayerId(null);
        playersService.calculatePlayersPoints();
    }
}
