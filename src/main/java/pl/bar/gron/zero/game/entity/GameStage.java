package pl.bar.gron.zero.game.entity;

public enum GameStage {
    WAITING_FOR_PLAYERS,
    GAME,
    END
}
