package pl.bar.gron.zero.game.entity;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.bar.gron.zero.cards.entity.Card;
import pl.bar.gron.zero.players.entity.OtherPlayer;
import pl.bar.gron.zero.players.entity.Player;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GameState {
    private Player player;
    private GameStage stage;
    private List<OtherPlayer> otherPlayers;
    private UUID currentPlayerId;
    private Map<Integer, Card> cardsOnTable;

    public GameState(GameState toCopy) {
        player = toCopy.player;
        stage = toCopy.stage;
        otherPlayers = toCopy.otherPlayers;
        currentPlayerId = toCopy.currentPlayerId;
        cardsOnTable = toCopy.cardsOnTable;
    }
}
