package pl.bar.gron.zero.cards.entity;

public enum CardColor {
    GREEN,
    RED,
    PINK,
    BLUE,
    GREY,
    BROWN,
    YELLOW;
}
