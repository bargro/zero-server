package pl.bar.gron.zero.cards.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Card {
    private CardColor color;
    private CardValue value;

    public static List<Card> getAllPosibleCards() {
        List<Card> cards = new ArrayList<>();
        Stream.of(CardValue.values())
              .forEach(v -> {
                  Stream.of(CardColor.values())
                        .forEach(c -> cards.add(new Card(c, v)));
              });
        return cards;
    }
}
