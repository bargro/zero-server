package pl.bar.gron.zero.cards.control;

import static java.util.Collections.emptyList;
import static java.util.stream.IntStream.range;
import static pl.bar.gron.zero.cards.entity.Card.getAllPosibleCards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import pl.bar.gron.zero.cards.entity.Card;

@ApplicationScoped
public class CardDrawer {
    private static final int NUMBER_OF_PLAYER_CARDS = 9;
    private static final int NUMBER_OF_TABLE_CARDS = 5;
    private List<Card> remainingCards;
    private Random random = new Random();

    @PostConstruct
    public void init() {
        remainingCards = getAllPosibleCards();
    }

    public Map<Integer, Card> drawCardsForPlayer() {
        return toMap(drawCards(NUMBER_OF_PLAYER_CARDS));
    }

    public Map<Integer, Card> drawCardsForTable() {
        return toMap(drawCards(NUMBER_OF_TABLE_CARDS));
    }

    private Map<Integer, Card> toMap(List<Card> cards) {
        Map<Integer, Card> cardsMap = new HashMap<>();
        for (int i = 0; i < cards.size(); i++) {
            cardsMap.put(i, cards.get(i));
        }
        return cardsMap;
    }

    private List<Card> drawCards(int expectedNumberOfCards) {
        if (expectedNumberOfCards > remainingCards.size()) {
            return emptyList();
        }
        List<Card> drawed = new ArrayList<>();

        range(0, expectedNumberOfCards).forEach(i -> {
            int index = random.nextInt(remainingCards.size());
            Card card = remainingCards.remove(index);
            drawed.add(card);
        });
        return drawed;
    }

}
