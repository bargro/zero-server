package pl.bar.gron.zero.players.entity;

public class PlayerNotFoundException extends RuntimeException {
    public PlayerNotFoundException(String msg) {
        super(msg);
    }

}
