package pl.bar.gron.zero.players.boundary;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import pl.bar.gron.zero.game.control.GameStateService;
import pl.bar.gron.zero.players.control.PlayersService;
import pl.bar.gron.zero.players.entity.Player;

@Slf4j
@Path("players")
public class PlayersResource {

    @Inject
    private PlayersService playersService;

    @Inject
    private GameStateService gameStateService;

    @POST
    @Produces(APPLICATION_JSON)
    public Response addPlayer(@QueryParam("name") @NotNull String name) {
        if (!playersService.isEnoughPlayersToGame()) {
            log.info("Nowy gracz {}", name);
            Player player = playersService.addPlayer(name);
            if (playersService.isEnoughPlayersToGame()) {
                gameStateService.beginGame();
            }
            return Response.ok()
                           .entity(player)
                           .build();
        } else {
            log.info("Próba dodania gracza {}. Jest już wystarczająca liczba graczy.", name);
            return Response.status(BAD_REQUEST)
                           .build();
        }
    }
}
