package pl.bar.gron.zero.players.control;

import static java.util.Optional.ofNullable;
import static java.util.UUID.fromString;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import lombok.extern.slf4j.Slf4j;
import pl.bar.gron.zero.cards.control.CardDrawer;
import pl.bar.gron.zero.cards.entity.Card;
import pl.bar.gron.zero.players.entity.OtherPlayer;
import pl.bar.gron.zero.players.entity.Player;

@ApplicationScoped
@Slf4j
public class PlayersService {
    private static final int NUMBER_OF_PLAYERS_TO_BEGIN = 3;
    private static final int NUMBER_OF_PAS_TO_BEGIN = 2;

    @Inject
    private CardDrawer cardDrawer;

    private Map<UUID, Player> players = new ConcurrentHashMap<>();
    private List<UUID> playerList = new ArrayList<>();

    private int nextPlayerIndex = 0;
    private UUID currentPlayer;

    public Player addPlayer(String name) {
        Player player = new Player(name, randomUUID(), cardDrawer.drawCardsForPlayer());
        players.put(player.getUuid(), player);
        log.info("Dodano nowego gracza {} aktualny stan {} ", player, listPlayers());
        return player;
    }

    public Optional<Player> getPlayer(String uuid) {
        return ofNullable(players.get(fromString(uuid)));
    }

    private String listPlayers() {
        StringBuilder sb = new StringBuilder();
        players.values()
               .forEach(e -> sb.append(e + "\n"));
        return sb.toString();
    }

    public List<OtherPlayer> getOtherPlayers(Player player) {
        return players.values()
                      .stream()
                      .filter(e -> !e.equals(player))
                      .map(this::toOtherPlayer)
                      .collect(toList());
    }

    private OtherPlayer toOtherPlayer(Player player) {
        return new OtherPlayer(player.getName(), player.getUuid(), player.getPasCounter(),
                player.getPoints());
    }

    public UUID getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean isEnoughPlayersToGame() {
        return players.size() == NUMBER_OF_PLAYERS_TO_BEGIN;
    }

    public void onBeginGame() {
        playerList.addAll(players.keySet());
        nextPlayer();
    }

    public void nextPlayer() {
        currentPlayer = playerList.get(nextPlayerIndex);
        log.info("NEXT PLAYER: {}", currentPlayer);
        nextPlayerIndex++;
        if (NUMBER_OF_PLAYERS_TO_BEGIN == nextPlayerIndex) {
            nextPlayerIndex = 0;
        }
    }

    public Card getCurrentPlayerCard(Integer cardKey) {
        Player player = players.get(currentPlayer);
        return player.getCard(cardKey);
    }

    public void onMove(Integer rejectedCardIndex, Card chosenCard) {
        Player player = players.get(currentPlayer);
        player.onMove(rejectedCardIndex, chosenCard);
    }

    public void pas() {
        players.get(currentPlayer)
               .onPas();
    }

    public boolean isEnoughPasToEnd() {
        Player player = players.get(currentPlayer);
        return NUMBER_OF_PAS_TO_BEGIN == player.getPasCounter();
    }

    public void calculatePlayersPoints() {
        players.values()
               .forEach(e -> {
                   new PointsCalculator().calculatePoints(e);
               });
    }
}
