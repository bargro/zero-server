package pl.bar.gron.zero.players.entity;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OtherPlayer {
    private String name;
    private UUID uuid;
    private int pasCounter;
    private int points;
}
