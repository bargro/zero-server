package pl.bar.gron.zero.players.control;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import pl.bar.gron.zero.cards.entity.Card;
import pl.bar.gron.zero.cards.entity.CardColor;
import pl.bar.gron.zero.cards.entity.CardValue;
import pl.bar.gron.zero.players.entity.Player;

public class PointsCalculator {
    public void calculatePoints(Player player) {
        List<Card> cards = new ArrayList<>(player.getCards()
                                                 .values());
        if (isZero(cards)) {
            player.setPoints(0);
            return;
        }
        removeFiveAndMoreCards(cards);
        ListMultimap<CardValue, Card> valuesMap = ArrayListMultimap.create();
        cards.forEach(card -> {
            valuesMap.put(card.getValue(), card);
        });

        int points = 0;

        for (CardValue value : valuesMap.keySet()) {
            if (!valuesMap.get(value)
                          .isEmpty()) {
                points += value.getValue();
            }
        }
        player.setPoints(points);
    }

    private boolean isZero(List<Card> cards) {
        ListMultimap<CardColor, Card> colorsMap = ArrayListMultimap.create();
        ListMultimap<CardValue, Card> valuesMap = ArrayListMultimap.create();

        cards.forEach(card -> {
            colorsMap.put(card.getColor(), card);
        });

        cards.forEach(card -> {
            valuesMap.put(card.getValue(), card);
        });

        boolean colorsFive = false;
        for (CardColor color : colorsMap.keySet()) {
            if (colorsMap.get(color)
                         .size() == 5) {
                colorsFive = true;
                break;
            }
        }
        boolean valuesFive = false;
        for (CardValue value : valuesMap.keySet()) {
            if (valuesMap.get(value)
                         .size() == 5) {
                valuesFive = true;
                break;
            }
        }
        return colorsFive && valuesFive;
    }

    private void removeFiveAndMoreCards(List<Card> cards) {
        removeByColors(cards);
        removeByValues(cards);
    }

    private void removeByColors(List<Card> cards) {
        ListMultimap<CardColor, Card> colorsMap = ArrayListMultimap.create();

        cards.forEach(card -> {
            colorsMap.put(card.getColor(), card);
        });

        colorsMap.keySet()
                 .forEach(color -> {
                     List<Card> currentCards = colorsMap.get(color);
                     if (currentCards.size() >= 5) {
                         cards.removeAll(currentCards);
                     }
                 });
    }

    private void removeByValues(List<Card> cards) {
        ListMultimap<CardValue, Card> valuesMap = ArrayListMultimap.create();
        cards.forEach(card -> {
            valuesMap.put(card.getValue(), card);
        });
        valuesMap.keySet()
                 .forEach(color -> {
                     List<Card> currentCards = valuesMap.get(color);
                     if (currentCards.size() >= 5) {
                         cards.removeAll(currentCards);
                     }
                 });
    }
}
