package pl.bar.gron.zero.players.entity;

import java.util.Map;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.bar.gron.zero.cards.entity.Card;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Player {
    private String name;
    private UUID uuid;
    private Map<Integer, Card> cards;
    private int pasCounter;
    private int points;

    public Player(String name, UUID uuid, Map<Integer, Card> cards) {
        this(name, uuid, cards, 0, 0);
    }

    public void onMove(Integer rejectedCardIndex, Card chosenCard) {
        cards.put(rejectedCardIndex, chosenCard);
    }

    public Card getCard(Integer cardKey) {
        return cards.get(cardKey);
    }

    public void onPas() {
        pasCounter++;
    }
}
