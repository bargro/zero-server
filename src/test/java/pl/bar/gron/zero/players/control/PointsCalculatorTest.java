package pl.bar.gron.zero.players.control;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import pl.bar.gron.zero.cards.entity.Card;
import pl.bar.gron.zero.cards.entity.CardColor;
import pl.bar.gron.zero.cards.entity.CardValue;
import pl.bar.gron.zero.players.entity.Player;

public class PointsCalculatorTest {

    @Test
    public void shouldCalculetePointsPoperly1() {
        PointsCalculator pc = new PointsCalculator();
        Player player = new Player();
        Map<Integer, Card> cards = new HashMap<>();
        cards.put(1, new Card(CardColor.PINK, CardValue.ONE));
        cards.put(2, new Card(CardColor.BLUE, CardValue.ONE));
        cards.put(3, new Card(CardColor.GREEN, CardValue.FOUR));
        cards.put(4, new Card(CardColor.BLUE, CardValue.EIGHT));
        cards.put(5, new Card(CardColor.RED, CardValue.ONE));
        cards.put(6, new Card(CardColor.RED, CardValue.THREE));
        cards.put(7, new Card(CardColor.RED, CardValue.FOUR));
        cards.put(8, new Card(CardColor.RED, CardValue.SEVEN));
        cards.put(9, new Card(CardColor.RED, CardValue.EIGHT));
        player.setCards(cards);

        pc.calculatePoints(player);

        assertEquals(13, player.getPoints());
    }

    @Test
    public void shouldCalculetePointsPoperly2() {
        PointsCalculator pc = new PointsCalculator();
        Player player = new Player();
        Map<Integer, Card> cards = new HashMap<>();
        cards.put(1, new Card(CardColor.GREEN, CardValue.TWO));
        cards.put(2, new Card(CardColor.RED, CardValue.FIVE));
        cards.put(3, new Card(CardColor.GREY, CardValue.FIVE));
        cards.put(4, new Card(CardColor.BROWN, CardValue.THREE));
        cards.put(5, new Card(CardColor.YELLOW, CardValue.THREE));
        cards.put(6, new Card(CardColor.BLUE, CardValue.THREE));
        cards.put(7, new Card(CardColor.GREEN, CardValue.THREE));
        cards.put(8, new Card(CardColor.GREY, CardValue.THREE));
        cards.put(9, new Card(CardColor.RED, CardValue.TWO));
        player.setCards(cards);

        pc.calculatePoints(player);

        assertEquals(7, player.getPoints());
    }

    @Test
    public void onZeroShouldCalculateZero() {
        PointsCalculator pc = new PointsCalculator();
        Player player = new Player();
        Map<Integer, Card> cards = new HashMap<>();
        cards.put(1, new Card(CardColor.BLUE, CardValue.EIGHT));
        cards.put(2, new Card(CardColor.BLUE, CardValue.SEVEN));
        cards.put(3, new Card(CardColor.BLUE, CardValue.SIX));
        cards.put(4, new Card(CardColor.BLUE, CardValue.FIVE));
        cards.put(5, new Card(CardColor.BLUE, CardValue.THREE));
        cards.put(6, new Card(CardColor.YELLOW, CardValue.THREE));
        cards.put(7, new Card(CardColor.GREEN, CardValue.THREE));
        cards.put(8, new Card(CardColor.GREY, CardValue.THREE));
        cards.put(9, new Card(CardColor.RED, CardValue.THREE));
        player.setCards(cards);

        pc.calculatePoints(player);

        assertEquals(0, player.getPoints());
    }
}
